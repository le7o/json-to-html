import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ContentService } from './content.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

describe('AppComponent', () => {

  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let contentService: ContentService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        ContentService
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
    contentService = fixture.debugElement.injector.get(ContentService);

  }));

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should convert json to html', async(() => {
    component.convertJsonToHtml();
    fixture.detectChanges();
    expect(component);
  }));

});
