import { TestBed, inject } from '@angular/core/testing';

import { ContentService } from './content.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ContentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [ContentService]
    });
  });

  it('should be created', inject([ContentService], (service: ContentService) => {
    expect(service).toBeTruthy();
  }));
});
