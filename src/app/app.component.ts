import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import 'rxjs/add/operator/finally';

import { ContentService } from './content.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  content: string;

  @ViewChild('textarea') textArea: ElementRef;

  constructor(private contentService: ContentService) { }

  ngOnInit(): void {
    this.convertJsonToHtml();
  }

  /**
   * Конвертировать входящий json
   */
  public convertJsonToHtml(): void {
    this.getJson();
  }

  /**
   * Добавить результат в элемент textarea
   * @param content 
   */
  protected viewHtmlContent(content: string): void {
    const textarea = this.textArea.nativeElement;
    textarea.value = content;
  }

  /**
   * Получить json
   */
  protected getJson(): void {
    this.contentService.getJson()
      .subscribe(
        success => {
          if (success) {
            const html = this.jsonToHtml(success);
            this.viewHtmlContent(html);
            this.content = html;
          }
        },
        error => {
          alert(error);
        }
      );
  }

  /**
   * Конвертировать json
   * @param json 
   */
  protected jsonToHtml(json: any): string {
    let html = '';
    if (json instanceof Array) {
      html += `<ul>`;
      json.forEach(element => {
        html += `<li>`;
        for (let key of Object.keys(element)) {
          if (element[key] instanceof Array) {
            html += this.generateTag(key, this.jsonToHtml(element[key]));
          } else {
            html +=  this.generateTag(key, element[key]);
          }
        }
        html += `</li>`;
      });
      html += `</ul>`;
    } else if (json instanceof Object) {
      for (let key of Object.keys(json)) {
        if (json[key] instanceof Array) {
          html += this.generateTag(key, this.jsonToHtml(json[key]));
        } else {
          html +=  this.generateTag(key, json[key]);
        }
      }
    }

    return html;
  }

  /**
   * Генерация HTML тега
   * @param tag 
   * @param content 
   */
  protected generateTag(tag: string, content?: string): string {
    const element = document.createElement(tag.split(/\.|#/)[0]);

    const id = tag.match(/#([^.#]+)/g);
    if (id) {
      element.id = id[0].slice(1);
    }

    const classList = tag.match(/\.([^.#]+)/g);
    if (classList) {
      classList.forEach(function (cls) {
        element.classList.add(cls.slice(1));
      });
    }

    if (content) {
      element.innerHTML = this.escapeHtml(content);
    }

    return element.outerHTML;
  }

  protected escapeHtml (string): string {
    const entityMap = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#39;',
      '/': '&#x2F;',
      '`': '&#x60;',
      '=': '&#x3D;'
    };

    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
      return entityMap[s];
    });
  }
}
