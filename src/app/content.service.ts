import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';

import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

@Injectable()
export class ContentService {

  constructor(private httpClient: HttpClient) {
  }

  public getJson(): Observable<any> {
    return this.httpClient
      .get<any>('/source.json')
      .pipe(catchError(this.handleError));
  }

  protected handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      return new ErrorObservable(`Не удалось получить данные, пожалуйста проверьте соединение: ${error.error.message}`);
    } else {
      return new ErrorObservable(`Не удалось получить данные от сервера: ${error.error}`);
    }
  }
}
